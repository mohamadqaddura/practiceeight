
package demo.services.rentit;

import static org.junit.Assert.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

import demo.Application;
import demo.SimpleDbConfig;
import demo.integration.dto.PlantHireRequestResource;
import demo.integration.dto.PlantHireRequestResourceAssembler;
import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.models.PlantHireRequest;
import demo.services.PlantHireRequestManager;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, SimpleDbConfig.class})
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
public class PracticeEightTester {
	
	@Autowired
	private RentalService rentItProxy;
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	PlantHireRequestManager phrManager;
	
	private MockRestServiceServer mockServer;
	
	
	PlantHireRequestResourceAssembler phrAssembler = new PlantHireRequestResourceAssembler();
	
	@Before
    public void setup() {
        this.mockServer = MockRestServiceServer.createServer(this.restTemplate);
    }
	


	
	@Test
	public void testAcceptedCreatePurchaseOrder() throws Exception {
	    PlantResource plantResource = new PlantResource();
		ObjectMapper mapper = new ObjectMapper();
	    String host="http://localhost:9000";
        String port = "9000";
       
       //Plants 
        List <PlantResource> allPlants= new LinkedList<PlantResource>();
        
        PlantResource p = new PlantResource();
		p.add(new Link(host+"/rest/plants/10001", "self"));
        allPlants.add(p);
		mockServer.expect(
				requestTo(host+"/rest/plants")
		)
				.andExpect(method(HttpMethod.GET))
				//.andExpect(content().contentType("application/json;charset=UTF-8")) dont specify unless sending
				//.andRespond(
						//withCreatedEntity(new URI("http://localhost:9000/rest/pos/345")) only for post
						.andRespond(withSuccess(mapper.writeValueAsString(allPlants), MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON)
						);


//PHR
		PlantHireRequestResource phr = new PlantHireRequestResource();
		phr.add(new Link(host+"/rest/phrs/10001","self"));	
		phr.setStartDate(new LocalDate(2014, 10, 6).toDate());
		phr.setEndDate(new LocalDate(2014, 10, 10).toDate());
		//phr.setPurchaseOrder(po);
		mockServer.expect(
				requestTo(host+"/rest/phrs")
		)
				.andExpect(method(HttpMethod.POST))
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(content().string(mapper.writeValueAsString(phr)))
				.andRespond(
						withCreatedEntity(new URI(host+"rest/phrs/10001"))
						.body(mapper.writeValueAsString(phr))
						.contentType(MediaType.APPLICATION_JSON)
						);
		
		
		//approve PHR
		PurchaseOrderResource po = new PurchaseOrderResource();
		po.setPlant(p);
		po.setStartDate(new LocalDate(2014, 10, 6).toDate());
		po.setEndDate(new LocalDate(2014, 10, 10).toDate());
		phr.setPurchaseOrder(po);
		mockServer.expect(
				requestTo(host+"/rest/phrs")
		)
				.andExpect(method(HttpMethod.POST))
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(content().string(mapper.writeValueAsString(phr)))
				.andRespond(
						withCreatedEntity(new URI(host+"rest/phrs/10001"))
						.body(mapper.writeValueAsString(phr))
						.contentType(MediaType.APPLICATION_JSON)
						);
		
		
		
//Purchase ORder		

		mockServer.expect(
				requestTo(host+"/rest/pos")
		)
				.andExpect(method(HttpMethod.POST))
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(content().string(mapper.writeValueAsString(po)))
				.andRespond(
						withCreatedEntity(new URI(host+"rest/pos/10001"))
						.body(mapper.writeValueAsString(po))
						.contentType(MediaType.APPLICATION_JSON)
						);
		
		
		
		
		
		
		//plants
		List<PlantResource> queriedPlants = rentItProxy.getAllPlants();
		//plant
		PlantResource selectedPlant = queriedPlants.get(0);
		//create phr
	    PlantHireRequest phrReal = new PlantHireRequest();
	    phrReal.setEndDate(phr.getEndDate());
	    phrReal.setStartDate(phr.getStartDate());
	    phrReal.setPrice(phr.getCost());	
	    phrReal.setId(10001L);
	    PlantHireRequest temp = new PlantHireRequest();
	    
	    temp = phrManager.createPlantHireRequest(phrReal);	    
	    PlantHireRequestResource phrr = phrAssembler.toResource(temp); 
	    //approve phr
		PlantHireRequest _resultPHR = phrManager.approvePlantHireRequest(10001L);
	    PlantHireRequestResource resultPHR = phrAssembler.toResource(_resultPHR);
		//create purchase order
	    PurchaseOrderResource poReal = rentItProxy.createPurchaseOrder(selectedPlant, new LocalDate(2014, 10, 6).toDate(), new LocalDate(2014, 10, 10).toDate());
		resultPHR.setPurchaseOrder(poReal);
		
		
		
		
		//check what the real purchase order created agains the simulated one
		
		
		mockServer.verify();
		assertEquals(po, poReal);
		}
	
	@Test
	public void testRejectedCreatePurchaseOrder() throws Exception {
	    PlantResource plantResource = new PlantResource();
		ObjectMapper mapper = new ObjectMapper();
	    String host="http://localhost:9000";
        String port = "9000";
       
       //Plants 
        List <PlantResource> allPlants= new LinkedList<PlantResource>();
        
        PlantResource p = new PlantResource();
		p.add(new Link(host+"/rest/plants/10001", "self"));
        allPlants.add(p);
		mockServer.expect(
				requestTo(host+"/rest/plants")
		)
				.andExpect(method(HttpMethod.GET))
				//.andExpect(content().contentType("application/json;charset=UTF-8")) dont specify unless sending
				//.andRespond(
						//withCreatedEntity(new URI("http://localhost:9000/rest/pos/345")) only for post
						.andRespond(withSuccess(mapper.writeValueAsString(allPlants), MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON)
						);


//PHR
		PlantHireRequestResource phr = new PlantHireRequestResource();
		phr.add(new Link(host+"/rest/phrs/10001","self"));	
		phr.setStartDate(new LocalDate(2014, 10, 6).toDate());
		phr.setEndDate(new LocalDate(2014, 10, 10).toDate());
		//phr.setPurchaseOrder(po);
		mockServer.expect(
				requestTo(host+"/rest/phrs")
		)
				.andExpect(method(HttpMethod.POST))
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(content().string(mapper.writeValueAsString(phr)))
				.andRespond(
						withCreatedEntity(new URI(host+"rest/phrs/10001"))
						.body(mapper.writeValueAsString(phr))
						.contentType(MediaType.APPLICATION_JSON)
						);
		
		
		//approve PHR
		PurchaseOrderResource po = new PurchaseOrderResource();
		po.setPlant(p);
		po.setStartDate(new LocalDate(2014, 10, 6).toDate());
		po.setEndDate(new LocalDate(2014, 10, 10).toDate());
		phr.setPurchaseOrder(po);
		mockServer.expect(
				requestTo(host+"/rest/phrs")
		)
				.andExpect(method(HttpMethod.POST))
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(content().string(mapper.writeValueAsString(phr)))
				.andRespond(
						withCreatedEntity(new URI(host+"rest/phrs/10001"))
						.body(mapper.writeValueAsString(phr))
						.contentType(MediaType.APPLICATION_JSON)
						);
		
		
		
//Purchase ORder		

		mockServer.expect(
				requestTo(host+"/rest/pos")
		)
				.andExpect(method(HttpMethod.POST))
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(content().string(mapper.writeValueAsString(po)))
				.andRespond(
						withCreatedEntity(new URI(host+"rest/pos/10001"))
						.body(mapper.writeValueAsString(po))
						.contentType(MediaType.APPLICATION_JSON)
						);
		
		
		
		
		
		
		//plants
		List<PlantResource> queriedPlants = rentItProxy.getAllPlants();
		//plant
		PlantResource selectedPlant = queriedPlants.get(0);
		//create phr
	    PlantHireRequest phrReal = new PlantHireRequest();
	    phrReal.setEndDate(phr.getEndDate());
	    phrReal.setStartDate(phr.getStartDate());
	    phrReal.setPrice(phr.getCost());	
	    phrReal.setId(10001L);
	    PlantHireRequest temp = new PlantHireRequest();
	    
	    temp = phrManager.createPlantHireRequest(phrReal);	    
	    PlantHireRequestResource phrr = phrAssembler.toResource(temp); 
	    //approve phr
		PlantHireRequest _resultPHR = phrManager.approvePlantHireRequest(10001L);
	    PlantHireRequestResource resultPHR = phrAssembler.toResource(_resultPHR);
		//create purchase order
	    PurchaseOrderResource poReal = rentItProxy.createRejectedPurchaseOrder(selectedPlant, new LocalDate(2014, 10, 6).toDate(), new LocalDate(2014, 10, 10).toDate());
		resultPHR.setPurchaseOrder(poReal);
		
		
		
		
		//check what the real purchase order created agains the simulated one
		
		mockServer.verify();
		assertEquals(po, poReal);
		}
	
	
}
