package demo;
import java.net.URI;
import java.net.URISyntaxException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleDbConfig {

    @Bean
    public DataSource dataSource()  {    

        URI dbUri;
        try {
            String username = "postgres";
            String password = "P!P@A@Q%";
            String url = "jdbc:postgresql://localhost:5432/zeroBuildIt";
            String dbProperty = System.getenv("DATABASE_URL");
            
            System.out.println(dbProperty);
            
            if(dbProperty != null) {
                dbUri = new URI(dbProperty);
                
        		String[] array = dbUri.getUserInfo().split(":");
                username = array[0];
                password = array.length > 1 ? array[1] : "";
                
                url = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();
            }     

            BasicDataSource basicDataSource = new BasicDataSource();
            basicDataSource.setUrl(url);
            basicDataSource.setUsername(username);
            basicDataSource.setPassword(password);

            return basicDataSource;

        } catch (URISyntaxException e) {
            return null;
        }
    }
}